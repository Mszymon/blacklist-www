angular.module('blacklist')
.config(function($routeProvider){
	$routeProvider
	.when('/', {
		redirectTo:'/email'
	})
	.when('/bank', {
		templateUrl:'template/page/bank/bank.html',
		controller: ['$http','$scope', function($http, $scope){
			var bank = $scope;
			$scope.products = [];
		    //sstart pagesize
		    $scope.currentPage = 1;
		    $scope.pageSize = 5;
		    $scope.empty = false;
		    //menu active
		    $scope.setTab = function(newValue){
		    	$scope.pageSize = newValue;
		    };
		    $scope.isSet = function(tabName){
		        return $scope.pageSize === tabName;
		    };
		    //Wczytanie danych
		    $scope.reolad = function(){
				$http.get('http://localhost:3000/bank')
				.success(function(data){
					console.log("Get bank success");
					if(data === "Brak danych"){
						$scope.empty = true;
						$scope.products = null;
					}else{
						$scope.products = data;
					}
			    }).error(function(error){
			    	console.log("Get bank error");
		    		console.log(error);
		    	});
			};
			//dodawanie wierszy
		    $scope.adddata = function(value1, value2) {
		    	var dane = {
					value : value1,
					type : value2,
					date : Date.now()
				};
		    	console.log(dane);
				$http.post('http://localhost:3000/addbank', dane)
				.success(function(data){
				    console.log("Add bank account success");
				    $scope.my_form.$setPristine();
//				    $scope.my_form.$setUntouched();
				    $scope.codecard = '';
				    $scope.cardtype = '';
				    $scope.submitted = false;
				    $scope.empty = false;
				    $scope.reolad();
				}).error(function(error){
					console.log("Add bank error");
				});
			};
			$scope.remove = function(index){
//				$scope.products.splice(index, 1);
				console.log(index);
				$http({
					url: 'http://localhost:3000/deletebank/'+index,
		            method: "DELETE",
		            headers: {'Content-Type': 'application/json'}
				}).success(function(data){
			    	console.log("Delete bank account success");
			    	$scope.reolad();
				}).error(function(error){
		    		console.log("Delete bank account error");
		    	});
			};
			$scope.reolad();
		}]
	})
	.when('/email', {
		templateUrl:'template/page/email/email.html',
		controller: ['$http','$scope', function($http, $scope){
			var email = $scope;
			$scope.empty = false;
		    $scope.products = [];
		    //sstart pagesize
		    $scope.currentPage = 1;
		    $scope.pageSize = 5;
		    //menu active
		    $scope.setTab = function(newValue){
		    	$scope.pageSize = newValue;
		    };
		    $scope.isSet = function(tabName){
		        return $scope.pageSize === tabName;
		    };
		    //Wczytanie danych
		    $scope.reolad = function(){
				$http.get('http://localhost:3000/email')
				.success(function(data){
			    	console.log("Get email success");
			    	if(data === "Brak danych"){
			    		$scope.empty = true;
						$scope.products = null;
					}else{
						$scope.products = data;
					}
			    }).error(function(){
		    		console.log("Get email error");
		    	});
			};
			//dodawanie wierszy
		    $scope.adddata = function(value1) {
		    	var dane = {
					value : value1,
					date : Date.now()
				};
		    	console.log(dane);
				$http.post('http://localhost:3000/addemail', dane)
				.success(function(data){
				    console.log("Add email success");
				    $scope.empty = false;
				    $scope.my_form.$setPristine();
//				    $scope.my_form.$setUntouched();
				    $scope.email = '';
				    $scope.submitted = false;
				    $scope.reolad();
				}).error(function(error){
					console.log("Add email error");
				});
			};
			//Usuwanie wierszy
			$scope.remove = function(index){
//				$scope.products.splice(index, 1);
				console.log(index);
				$http({
					url: 'http://localhost:3000/deleteemail/'+index,
		            method: "DELETE",
		            headers: {'Content-Type': 'application/json'}
				}).success(function(data){
			    	console.log("Delete email success");
			    	$scope.reolad();
				}).error(function(){
		    		console.log("Delete email error");
		    	});
			};
			$scope.reolad();
		}]
	})
	.when('/card', {
		templateUrl:'template/page/card/card.html',
		controller: ['$http', '$scope', function($http, $scope){
			var card = $scope;
			$scope.empty = false;
			$scope.products = [];
		    //sstart pagesize
		    $scope.currentPage = 1;
		    $scope.pageSize = 5;
		    //menu active
		    $scope.setTab = function(newValue){
		    	$scope.pageSize = newValue;
		    };
		    $scope.isSet = function(tabName){
		        return $scope.pageSize === tabName;
		    };
		    //Wczytanie wierszy
		    $scope.reolad = function(){
				$http.get('http://localhost:3000/card').success(function(data){
			    	console.log("Get card success");
			    	if(data === "Brak danych"){
			    		$scope.empty = true;
						$scope.products = null;
					}else{
						$scope.products = data;
					}
			    }).error(function(data){
		    		console.log("Get card error");
		    	});
			};
			//Dodawanie wierszy
			$scope.adddata = function(value1, value2) {
				var dane = {
					value : value1,
					type : value2,
					date : Date.now()
				};
				$http.post('http://localhost:3000/addcard', dane)
				.success(function(data){
				    console.log("Add card success");
				    $scope.empty = false;
				    $scope.my_form.$setPristine();
//				    $scope.my_form.$setUntouched();
				    $scope.codecard = '';
				    $scope.cardtype = '';
				    $scope.submitted = false;
				    $scope.reolad();
				}).error(function(error){
					console.log("Add card error");
				});
			};
			//Usuwanie wierszy
			$scope.remove = function(index){
//				$scope.products.splice(index, 1);
				console.log(index);
				$http({
					url: 'http://localhost:3000/deletecard/'+index,
		            method: "DELETE",
		            headers: {'Content-Type': 'application/json'}
				}).success(function(data){
			    	console.log("Delete card success");
			    	$scope.reolad();
				}).error(function(){
		    		console.log("Delete card error");
		    	});
			};
			$scope.reolad();
		}]
	})
	.otherwise({redirectTo:'/email'})
});