/**
 * 
 */

(function() {
	var app = angular.module("blacklist", [ 'ngRoute','ngMessages','angularUtils.directives.dirPagination']);
	app.controller("tabcontroller",['$scope', '$location', function($scope, $location) {
		$scope.isActive = function (viewLocation) {
		     var active = (viewLocation === $location.path());
		     return active;
		};
		
	}]);
	
})();
